#include <cstdio>
#include <cstdlib>
#include <iostream>

int main()
{
	int a, i, j, k, T, n, kuo[100001], ans[100001], tmp;
	scanf("%d", &T);
	while(T--){
		int max = 0, max_id, flag = 0;
		kuo[0] = -1;
		scanf("%d", &n);
		for(i = 1; i <= n; i++){
			scanf("%d", &a);
			ans[i] = -1;
			kuo[i] = a;
			if(max < a){
				max = a;
			}
		}
		for(i = 1;i <= n; i++){
			//printf("ans[%d] = %d\n",i - 1, ans[i - 1]);
			if(kuo[i] == max){					//the smartest
				ans[i] = 0;
				continue;
			}
			k = i - 1;
			if(k == 0)
				k = n;
			if(kuo[k] > kuo[i]){				//the next person is smarter
				ans[i] = k;
			}
			else{								//not smarter
				j = ans[k];
				while(j != -1){					//"jump" to find the person smarter than next person
					if(j == 0 || kuo[j] > kuo[i]){
						ans[i] = j;
						//flag = 1;
						break;
					}
					else{
						k = j;
						j = ans[k];
					}
				}
				if(ans[k] == -1){				//test person by person
					for(j = k; j != i; j--){
						if(j == 0)
							j = n;
						if(kuo[j] > kuo[i]){
							ans[i] = j;
							break;
						}
					}
				}
			}		
		}
		//printf("----------------\n");
		for(i = 1; i <= n - 1; i++){
			printf("%d ", ans[i]);
		}
		printf("%d\n", ans[n]);
		
	}
	return 0;
}