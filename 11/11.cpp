#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <time.h>
#include <algorithm>
#include <limits.h>
using namespace std;
#define int128 __uint128_t		//	define 128-bit integer

int128 adj_matrix[128];			//adjacancy matrix
int128 one = 1;
int maximum ;					//remember current max

int popcount(int128 num)		//count how many 1 int the integer
{
	return __builtin_popcountll(num >> 50) + __builtin_popcountll(num & ((one << 50) - 1));
}

int ctz(int128 val)				//count how many zero before first 1
{
 if((val & ((one << 50) - 1)) == 0)
    return __builtin_ctzll(val >> 50) + 50;
 return __builtin_ctzll(val);
}

void bronker(int ans, int128 p, int128 x)		//Bron–Kerbosch algorithm
{
	int pivot, i;
	int128 r;
	if(p == 0){
		if(x == 0){
			if(maximum < ans)
				maximum = ans;
			return;
		}
			return;
	}
	pivot = ctz(p | x);
	r = p & (~adj_matrix[pivot]);			//smaller p
	int count = popcount(p);
	while(r && (ans + count) > maximum){	//if the number cant be bigger than max => return
		i = ctz(r);
		if(i == pivot || (adj_matrix[i] & r))
			bronker((ans + 1), p & adj_matrix[i], x & adj_matrix[i]);
		x |= (one << i);
		r &= ~(one << i);
		p &= ~(one << i);
		count--;
	}
}

int main()
{
	int i, j, T, n, m, a, b;

	scanf("%d", &T);
	while(T--){
		maximum = 0;
		scanf("%d %d", &n, &m);
		for(i = 0; i < n; i++){
			adj_matrix[i] = (one << n) - 1;
			adj_matrix[i] &= ~(one << i);
		}
		for(i = 0; i < m; i++){
			scanf("%d %d", &a, &b);
			adj_matrix[a] &= ~(one << b);
			adj_matrix[b] &= ~(one << a);
		}
		bronker(0, (one << n) - 1 ,0);
		printf("%d\n", maximum);
	}
	return 0;
}