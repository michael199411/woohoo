#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <time.h>
#include <algorithm>
using namespace std;

struct node
{
	int id;
	int degree;
	int rand;
};

bool comp(const node &a, const node &b)
{
	if(a.degree < b.degree)
		return true;
	else if(a.degree == b.degree){
		return(a.rand > b.rand);
	}
	else
		return false;
}

int main()
{
	int i, j, k, l, w, T, n, m, a, b, vertex, deleted, num, tmp;
	vector< vector< int > > adj(200);
	//vector< int > test(100);
	vector < node > deg(200);
	scanf("%d", &T);
	while(T--){
		num = 0;
		srand(time(NULL));
		scanf("%d %d", &n, &m);
		vertex = n;
		for(i = 0; i < m; i++){
			scanf("%d %d", &a, &b);
			adj[a].push_back(b);
			adj[b].push_back(a);
		}
		for(i = 0; i < n; i++){
			deg[i].degree = adj[i].size();
	//		printf("d = %d, id = %d\n", adj[i].size(), i);
			deg[i].id = i;
			deg[i].rand = rand() % 500;
		}
	//	printf("v = %d\n", vertex);
		sort(deg.begin(), deg.begin() + vertex, comp);
		
		for(i = 0; i < n; i++){
			for(j = 0; j < adj[i].size(); j++){
	//			printf("%d ", adj[i][j]);
			}
	//		printf("%d ", deg[i].degree);
	//		printf("\n");
		}
		while(vertex > 0){
	//		for(i = 0; i < vertex; i++)
	//			printf("(%d %d)",deg[i].id, deg[i].degree);
			i = 0;
	//		printf("vertices = %d\n", vertex);
	//		printf("node = %d, deg = %d\n", deg[i].id, deg[i].degree);
			//ans.push_back(deg[i].id);
			num++;
			tmp = deg[i].degree;
			for(j = 0; j < tmp ; j++){
				deleted = adj[deg[i].id][j];
	//			printf("delete = %d\n", deleted);
				for(k = 0; k < adj[deleted].size(); k++){
					for(l = 0; l < vertex; l++){
						if(deg[l].id == adj[deleted][k]){
							//printf("adj with del = %d\n", deg[l].id);
							deg[l].degree--;
							//printf("sizer = %d\n", adj[deg[l].id].size());
							for(w = 0; w < adj[deg[l].id].size(); w++){
								if(adj[deg[l].id][w] == deleted){
							//		printf("erase %d in %d\n", adj[deg[l].id][w], deg[l].id);
									adj[deg[l].id].erase(adj[deg[l].id].begin() + w);
									break;
								}
							}
						}
					}
				}
				for(k = 0; k < vertex; k++){
					if(deg[k].id == deleted){
						deg.erase(deg.begin() + k);
						break;
					}
				}
				vertex--;
			}
			//printf("before = %d\n", deg.size());
			deg.erase(deg.begin());
			//printf("after = %d\n", deg.size());
			vertex--;
			sort(deg.begin(), deg.begin() + vertex, comp);
			for(i = 0; i < vertex; i++){
				//printf("(%d %d)\n",deg[i].id, deg[i].degree);
			}
		}
		printf("%d\n", num);
		for(i = 0; i < 200; i++){
			adj[i].clear();
			deg[i].id = 0;
			deg[i].degree = 0; 
		}
	}
	return 0;
}