#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
using namespace std;
long long int N = 10000000000; //設成merge sort停止條件 因為所有輸入都不會大於它

struct box{						//存盒子長寬
	long long int length;
	long long int width;
};


bool comp(const box &a,const box &b)		//sort時按照常邊由小到大排好 長邊一樣時 照短邊由小到大排
{
	if(a.length < b.length ) return 1;
	else if(a.length == b.length && a.width < b.width) return 1;
	else return 0;
}

long long int merge(vector< box > &a, int left , int right)
{
	if(left >= right){		//終止條件
		return 0;
	}
	else{
		int mid = (left + right) / 2;
		int l_len, r_len;
		int i, j, k, tmp_i;
		long long int tmp;
		long long int l_count, r_count, count = 0;
		box solid;
		solid.width = N;
		solid.length = N;
		vector< box > l_array, r_array;

		l_count = merge(a, left, mid);		//左半邊陣列遞迴merge sort 回傳值為有幾個箱子可以互相放
		r_count = merge(a, mid + 1, right); //右半邊merge sort
		count = l_count + r_count;
		l_len = mid - left + 1;				//左陣列長度
		r_len = right - mid;				//右陣列長度
		for(i = 0; i < l_len; i++){			//將元素存入左, 右陣列(l_array, r_array)
			l_array.push_back(a[left + i]);
		}
		for(i = 0; i < r_len; i++){
			r_array.push_back(a[mid + 1 + i]);	
		}
		l_array.push_back(solid);			//在最後面放入很大的數 讓merge sort可以順利執行
		r_array.push_back(solid);
		i = 0;
		j = 0;
		
		for(k = left; k <= right; k++){
			if(i >= l_len && j >= r_len){	//跑完左右陣列則跳出迴圈
				break;
			}								//做merge sort 若左陣列中元素較小 則count要往上加所有比它大的盒子的數量
			if((l_array[i].width < r_array[j].width) or (l_array[i].width == r_array[j].width && l_array[i].length != r_array[j].length)){
				a[k] = l_array[i];
				i++;
				count += r_len - j;
			}
			else if(l_array[i].width > r_array[j].width){	//右陣列中元素較小 則不update count
				a[k] = r_array[j];
				j++;
			}
			else{						//判斷相同的盒子
				tmp_i = i;
				int tmp_j = j;
				tmp = 1;
				while(1){
					if((l_array[i].width == r_array[j].width) && (l_array[i].length == r_array[j].length)){	//若左右為相同盒子 則右邊繼續往後檢查
						a[k] = r_array[j];
						j++;
						k++;
					}
					else{												//若右邊出現不相同的盒子 則檢查左邊有幾個和之前相同的盒子
						if((l_array[tmp_i + 1].width == l_array[i].width) && (l_array[tmp_i + 1].length == l_array[i].length)){
							tmp++;
							tmp_i++;
						}
						else
							break;
					}
				}
				count += (tmp * (j - tmp_j) * 2);		//將左右相同盒子的數目相乘後再乘2 之後右邊從不相同的盒子開始檢查
				k--;									//因為進for迴圈前會先k++ 會造成多跳一格 故先將k - 1
			}
		}
		return count; 
	}
}
int main()
{
	int t, num;
	int i, j;
	long long int sum;
	long long int ta, tb;
	
	scanf("%d", &t);
	for(i = 0;i < t; i++){
		box a[200000];			//輸入的盒子
		vector< box > boxes;
		scanf("%d", &num);
		for(j = 0; j < num; j++){
			scanf("%lld%lld", &ta, &tb);
			if(ta < tb) {
				a[j].length = tb;
				a[j].width = ta;
			}
			else{
				a[j].length = ta;
				a[j].width = tb;
			}
			boxes.push_back(a[j]);	//將所有盒子放到vector裡
		}
		sort(boxes.begin(), boxes.end(), comp);	//照長邊排序
		sum = merge(boxes, 0 , num - 1);		
		printf("%lld\n", sum);	
	}
	return 0;
}