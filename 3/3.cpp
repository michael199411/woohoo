#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <vector>
using namespace std;


struct eve{			//存事件的l, r, v
	int left;
	int right;
	int v;
};

struct tags{		//礦坑與事件編號 還有在算delta時需要用到的v值
	double pri;
	int v;
};

struct play{				//存每個玩家的編號目標和當前金塊
	int id;					//玩家編號
	long long int goal;		//目標
	long long int have;		//當前金塊
};

bool comp(const tags &a, const tags &b) //sort時用編號大小排序
{
	if(a.pri < b.pri) return 1;
	else return 0;
}

vector< int > mine2player(200000);	//礦坑對照玩家的列表

void binary_search(vector<long long int> &delta, vector< vector< int > > &player2mine, vector< play > &players, vector< eve > &events, vector< int > &ans, int left, int right)
{
		if(left > right)	
			return;

		vector< tags > buffer;
		int i, j;
		long long point = 0;
		int mid = (left + right) / 2;
		tags tp;
		tp.pri = 0;
		tp.v = 0;
		play tmp;
		tags tmp2;
		tmp.goal = tmp.have = tmp.id = 0;		//初始化
		vector< play > success, fail;			//從1開始
		success.push_back(tmp);
		fail.push_back(tmp);
		for(i = 1; i < players.size(); i++){	//玩家人數
			for(j = 0; j < player2mine[players[i].id].size(); j++){ // 該玩家礦坑數
				tmp2.pri = player2mine[players[i].id][j];
				tmp2.v = 0;
				delta[players[i].id] = 0;			//將該玩家的delta初始化 	
				buffer.push_back(tmp2);				//丟進LIST
			}
		}
		if(buffer.size() == 0) return;				//如果沒有玩家 直接return	
		for(i = left; i <= mid; i++){				//把事件丟進LIST
			tp.pri = (double)events[i].left - 0.5;	//將事件的編號換算成浮點數
			tp.v = events[i].v;
			buffer.push_back(tp);
			tp.pri = (double)events[i].right + 0.5;
			tp.v = -(events[i].v);
			buffer.push_back(tp);
		}

		sort(buffer.begin(), buffer.end(), comp);
		for(i = 0; i < buffer.size(); i++){	// 算DELTA
			if(buffer[i].v != 0){ 
				point += buffer[i].v; 		 //碰到事件 更新point值
			}
			else{
				delta[mine2player[buffer[i].pri]] += point;	//碰到礦 則delta往上加
			}
		}
		for(i = 1; i < players.size(); i++){	//檢查每個玩家是否達到目標		
			if(players[i].have + delta[players[i].id] >= players[i].goal){	//成功
				ans[players[i].id] = mid;		//更新答案
				success.push_back(players[i]);	//不管delta 將該玩家存到success陣列 
			}
			else{		//失敗
				players[i].have += delta[players[i].id];	//代表還沒成功 把ＤＥＬＴＡ加上去
				fail.push_back(players[i]);					//將該玩家存入fail陣列
			}
		}
		if(left == right){		//終止
			return;
		}
		buffer.clear(); //清空buffer
		binary_search(delta, player2mine, success, events, ans, left, mid);	//遞迴
		binary_search(delta, player2mine, fail, events, ans, mid + 1, right);	
}
 
int main()
{
	int i, j, k, l, T;
	tags tpp;
	int num_p, num_m, num_e, tmp;

	
	scanf("%d", &T);
	for(l = 0; l < T; l++){
		scanf("%d%d%d", &num_p, &num_m, &num_e);
		vector< eve > events(num_e + 1);					//存所有事件
		vector< vector< int> > player2mine(num_p + 1);		//玩家對應到礦坑的表
		vector< play > players(num_p + 1);					//存每個玩家的狀態
		vector< int > ans(num_p + 1, -1);					//存該玩家在第幾個事件成功 初始值為-1
		vector< long long int > delta(num_p + 1, 0);		//在檢查是否達到goal時用來暫存的delta

		for(i = 1; i <= num_p; i++){						//讀取輸入 存入陣列中 都從一開始
			scanf("%lld", &players[i].goal);
			players[i].have = 0;
			players[i].id = i;
		}
		for(i = 1; i <= num_m; i++){
			scanf("%d", &tmp);
			mine2player[i] = tmp;
			player2mine[tmp].push_back(i);
		}
		for(i = 1; i <= num_e; i++){
			scanf("%d%d%d", &events[i].left, &events[i].right, &events[i].v);
		}

		binary_search(delta, player2mine, players, events, ans, 1, num_e);
		for(i = 1; i < ans.size(); i++){	//輸出答案
			if(i == ans.size() - 1)
				printf("%d\n", ans[i]);
			else
				printf("%d ", ans[i]);
		}
		delta.clear();				//清空
		ans.clear();
		mine2player.clear();
	}
	return 0;
}
