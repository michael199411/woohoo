#include <cstdio>
using namespace std;

int main(void)
{
	int T;
	scanf("%d", &T);
	while(T--){
		int i, j, n, m, q;
		int l, r, v;
		scanf("%d %d %d", &n, &m, &q);
		int goal[n + 1], gold[n + 1];
		int block[m + 1], answer[n + 1];
		for(i = 1; i <= n; i++){
			scanf("%d", &(goal[i]));
			gold[i] = 0;
			answer[i] = 0;
		}	
		for(i = 1; i <= m; i++)
			scanf("%d", &(block[i]));
		for(i = 1; i <= q; i++){
			scanf("%d %d %d", &l, &r, &v);
			for(j = l; j <= r; j++){
				gold[block[j]] += v;
				if(gold[block[j]] >= goal[block[j]] && answer[block[j]] == 0)
					answer[block[j]] = i;
			}
		}
		for(i = 1; i <= n; i++)
			if(answer[i] == 0)
				answer[i] = -1;
		for(i = 1; i < n; i++)
			printf("%d ",answer[i]);
		printf("%d\n",answer[i]);
	}
	return 0;
}