#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAX 1000
int main() {
	
	int T, n, m, q, l, r;
	srand(time(NULL));
	scanf("%d", &T);
	scanf("%d", &n);
	scanf("%d", &m);
	scanf("%d", &q);
	printf("%d\n", T);
	while(T--){
		printf("%d %d %d\n", n, m, q);
		for(int i = 1; i < n; ++i)
			printf("%d ", rand() % MAX + 1);
		printf("%d\n", rand() % MAX + 1);
		for(int i = 1; i < m; ++i)
			printf("%d ", rand() % n + 1);
		printf("%d\n", rand() % n + 1);
		for(int i = 0; i < q; ++i){
			l = rand() % (m - 1) + 1;
			r = l + rand() % (m - l) + 1;
			printf("%d %d %d\n", l, r, rand() % MAX + 1);
		}
	}
	return 0;
}
