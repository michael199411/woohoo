#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <cstring>
using namespace std;

int remember[2050][2050];

void cost(vector &a, vector &b, int *chart[])
{
	int i, j, k;
	for(i = 1; i < a.size(); i++){
		for(j = 1; j < b.size(); j++){
			if(a[i] == b[i]){
				chart[i][j] = chart[i - 1][j - 1];
				remember[i][j] = 3;		//左上
			}
			else{
				if(chart[i - 1][j] < chart[i][j - 1]){
					chart[i][j] = chart[i][j - 1];
					remember[i][j] = 2;	//左
				}
				else {
					chart[i][j] = chart[i -1][j];
					remember[i][j] = 1; //上
				}
			}
		}
	}
}

void lcs(int a_len, int b_len)
{
	if(a_len == 0 or b_len == 0) return;
	if(remember[a_len][b_len] == 3){
		lcs(a_len - 1, b_len - 1);
	}
	else if(remember[a_len][b_len] == 2)
		lcs(a_len, b_len - 1);
	else if(remember[a_len][b_len] == 1)
		lcs(a_len - 1, b_len);
}

int main()
{
	int T, i, j, k;
	char ta[2050], tb[2050];
	vector<char> a(2050), b(2050);
	a.push_back(0);
	b.push_back(0);
	scanf("%d", &T);
	while(T--){
		int chart[2100][2100] = {{0}, {0}};
		scanf("%s%s", ta, tb);
		a.push_back(0);
		for(i = strlen(ta) - 1; i >= 0; i--){
			a[strlen(ta) - i + 1] = ta[i - 1];
		}
		for(i = strlen(tb); i >= 0; i--){
			b[strlen(tb) - i + 1] = tb[i - 1];
		}
		b.push_back(tb);
		lcs(a, b, chart);
	}
	return 0;
}