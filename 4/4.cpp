#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <cstring>
using namespace std;

int remember[2050][2050];
vector<char> ans;

struct dp{
	int chart[2050][2050];
	char chartA[2050][2050];
};
void lcs(vector<char> &a, vector<char> &b, int i, int j, dp &dpro)
{
//	cout<<i<<" "<<j<<"\n";
	if(i == 0 or j == 0) {
	//	cout<<"rrr\n";
		return;
	}
	if(remember[i][j] == 3){
		lcs(a, b, i - 1, j - 1, dpro);
	//	cout<<"ans = "<<a[i]<<"\n";
	//	cout<<"i = "<<i<<" "<<"j = "<<j<<"\n";
		ans.push_back(a[i]);
	}
	else{
	//	cout<<"in else\n";
	//	cout<<"s1 = "<<a[i]<<" "<<"s2 = "<<b[j]<<"\n";
		if(a[i] == dpro.chartA[i][j])
			lcs(a, b, i, j - 1, dpro);
		else if(b[j] == dpro.chartA[i][j])
			lcs(a, b, i - 1, j, dpro);
		else
			lcs(a, b, i - 1, j, dpro);
	}
}

void cost(vector<char> &a, vector<char> &b, int al, int bl)
{
	int i, j, k;
	dp dpro;
	for(i = 1; i <= al; i++){
		for(j = 1; j <= bl; j++){
			if(a[i] == b[j]){
				dpro.chart[i][j] = dpro.chart[i - 1][j - 1] + 1;
				dpro.chartA[i][j] = a[i];
				//cout<<"A = "<<dpro.chartA[i][j]<<"\n";
				//cout<<"i = "<<i<<" "<<"j ="<<j<<"\n" ;
				remember[i][j] = 3;		//左上
			}
			else{
				if(dpro.chart[i - 1][j] < dpro.chart[i][j - 1]){
					dpro.chart[i][j] = dpro.chart[i][j - 1];
					remember[i][j] = 2;	//左
					dpro.chartA[i][j] = dpro.chartA[i][j - 1];
				}
				else if(dpro.chart[i - 1][j] > dpro.chart[i][j - 1]){
					dpro.chart[i][j] = dpro.chart[i - 1][j];
					dpro.chartA[i][j] = dpro.chartA[i - 1][j];
					remember[i][j] = 1;	//上
				}
				else{
					remember[i][j] = 4;
					dpro.chart[i][j] = dpro.chart[i - 1][j];
					if(((dpro.chartA[i - 1][j] < dpro.chartA[i][j - 1]) && dpro.chartA[i - 1][j] != 0) || ((dpro.chartA[i][j - 1] < dpro.chartA[i - 1][j]) && dpro.chartA[i][j - 1] == 0))
						dpro.chartA[i][j] = dpro.chartA[i - 1][j];
					else 
						dpro.chartA[i][j] = dpro.chartA[i][j - 1];
				}
			}
		}
	}
	lcs(a, b, al, bl, dpro);
	return;
}

int main()
{
	int T, i, j, k;
	char ta[2050], tb[2050];
	
	scanf("%d", &T);
	while(T--){
		scanf("%s%s", ta, tb);
		vector<char> a(2050), b(2050);
		for(i = strlen(ta) - 1; i >= 0; i--){
			a[strlen(ta) - i] = ta[i];
	//		cout<<a[strlen(ta) - i]<<" ";
		}
	//	cout<<"\n";
		for(i = strlen(tb) - 1; i >= 0; i--){
			b[strlen(tb) - i] = tb[i];
	//		cout<<b[strlen(tb) - i]<<" ";
		}
	//	cout<<"\n";
	//	cout<<strlen(ta)<<" "<<strlen(tb)<<"\n";
		cost(a, b, strlen(ta), strlen(tb));
		for(j = 0; j < ans.size(); j++)
			printf("%c", ans[ans.size() - j - 1]);
		printf("\n");
		ans.clear();
	}
	return 0;
}