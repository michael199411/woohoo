#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
using namespace std;
int DP[16][17][32800];
int main()
{
	int i, j, k, T;
	int n,m;
	char tmp1[16];
	int tmp, tmp2;
	scanf("%d", &T);
	while(T--){
		for(i = 0; i < 16; i++){
			for(j = 0; j < 17; j++){
				for(k = 0; k < 32800; k++){
					DP[i][j][k] = 0;
				}
			}
		}
		scanf("%d%d", &n, &m);
		bool map[n + 1][m + 1];
		for(i = 0; i <= n; i++)
			map[i][0] = 1;
		for(i = 0; i <= m; i++)
			map[0][i] = 1;
		for(i = 1; i <= n; i++){
			scanf("%s", tmp1);
			for(j = 0; j < m; j++){
				if(tmp1[j] == '.')
					map[i][j + 1] = 0;
				else
					map[i][j + 1] = 1;
			}
		}
		/*for(i = 0; i <= n; i++){
			for(j = 0; j <= m; j++)
				cout<<map[i][j]<<" ";
			cout<<"\n";
		}*/
		for(i = 1; i <= n; i++){
			for(j = 1; j <= m; j++){
		//		cout<<"i, j"<<i<<j<<"\n";
				for(k = 0; k < (1 << m); k++){
		//			cout<<"k = "<<k<<"dp = "<<DP[i][j][k]<<"\n";
					tmp = k >> (j - 1);
					if(map[i][j] == 1){
						if(tmp % 2 == 1){  //1X1
							DP[i][j + 1][k] += DP[i][j][k]; 	
							if(DP[i][j + 1][k] == 0)
								DP[i][j + 1][k] = 1;
							DP[i][j + 1][k] %= (1000000000 + 7);
						}
					}
					else{
						if(tmp % 2 == 1){  //1X1
		//					cout<<i<<j<<k<<"  1X1\n";
							DP[i][j + 1][k] += DP[i][j][k]; 	
							if(DP[i][j + 1][k] == 0)
								DP[i][j + 1][k] = 1;
							DP[i][j + 1][k] %= (1000000000 + 7);
						}
						if((tmp % 2 == 0) && (map[i - 1][j] != 1)){ //2X1
		//					cout<<i<<j<<k<<"  2X1\n";
							tmp2 = k | (1 << (j - 1));
							DP[i][j + 1][tmp2] += DP[i][j][k];
							if(DP[i][j + 1][tmp2] == 0)
								DP[i][j + 1][tmp2] = 1;
							DP[i][j + 1][tmp2] %= (1000000000 + 7);
						}
						if((tmp % 2 == 1) && ((tmp >> 1) % 2 == 1) && (j != m) && (map[i][j + 1] != 1)){ //1X2
		//					cout<<i<<j<<k<<"  1X2\n";
							DP[i][j + 2][k] += DP[i][j][k];
							if(DP[i][j + 2][k] == 0)
								DP[i][j + 2][k] = 1;
							DP[i][j + 2][k] %= (1000000000 + 7);
						}
						if((tmp % 2 == 0) && ((tmp >> 1) % 2 == 0) && (i != 1) && (j != m)){
						 	if(map[i - 1][j] == 0 && map[i - 1][j + 1] == 0 && map[i][j + 1] == 0){ //2X2
		//				 	cout<<i<<j<<k<<"  2X2\n";
							int a = 1 << (j - 1);
							int b = 1 << j;
							int c = a + b;
							tmp2 = k | c;
							DP[i][j + 2][tmp2] += DP[i][j][k];
							if(DP[i][j + 2][tmp2] == 0)
								DP[i][j + 2][tmp2] = 1;
							DP[i][j + 2][tmp2] %= (1000000000 + 7);
							}
						}
						if(tmp % 2 == 1){ //0X0
		//					cout<<i<<j<<k<<"  zero\n";
							tmp2 = k ^ (1 << (j - 1));
							DP[i][j + 1][tmp2] += DP[i][j][k];
							if(DP[i][j + 1][tmp2] == 0)
								DP[i][j + 1][tmp2] = 1;
							DP[i][j + 1][tmp2] = DP[i][j + 1][tmp2] % (1000000000 + 7);
						}
					}
				}
		//		cout<<"-----\n";
			}
			if(i != 15)
				for(int l = 0; l < (1<<m); l++){
					DP[i + 1][1][l] = DP[i][m + 1][l];
				}
			if(i == n && j == (m + 1))
				printf("%d\n", DP[n][m + 1][(1<<m) - 1]);

		}	

	}
	return 0;
}