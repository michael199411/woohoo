#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

int center(int c, int location[], int K, int N)
{
	int area = 1, l = 1, i;
	for(i = 1; i <= N, i++){
		if(location[i] - location[l] > c){
			l = i;
			area++;
		}
		if(area >= k)
			return area;
	}
	return area;
}

int b_search(int left, int right, int K, int N, int location[])
{
	int mid, cur;
	int i, j, c;
	while(left <= right){
		mid = (left + right) / 2;
		c = center(mid, location, K, N);
		if(c < K){
			right = mid - 1;
			cur = mid;
		}
		else{
			left = mid + 1;
		}
	}
	return cur;
}


int main()
{
	int i, j, T, N, K;
	scanf("%d", &T);
	while(T--){
		scanf("%d%d", &N, &K);
		int location[N + 1];
		for(i = 1; i <= N; i++){
			scanf("%d", &location[i]);
		}
		int ans = b_search(0, location[N], K, N, location);
	}
	return 0;
}