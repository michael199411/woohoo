#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>
using namespace std;

int center_num(int mid, vector<int> &location, int K, int N)
{
	int area = 1, l = 1, counter = 0;
	int i;
	for(i = 1; i <= N; i++){

		if((location[i] - location[l]) > mid ) {		//distance bigger than half of current max interval
			if(counter == 0){							//put the airport
				l = i - 1;
				counter = 1;
				i--;
			}
			else if(counter == 1){
				l = i;									//build a new interval
				area++;
				counter = 0;
			}
		}	
	}
	return area;
}

int b_search(int left, int right, int K, int N, vector<int> &location)
{
	int mid, cur = right;
	int i, j, c;
	while(left <= right){						//find the place of airport
		mid = (left + right) / 2 ;
		//printf("new mid = %d\n", mid);
		c = center_num(mid, location, K, N);	//test the num of interval
		//printf("interval = %d\n", c);
		if(c <= K){
			right = mid - 1;					//update right 
			cur = mid;
			//printf("cur = %d\n", cur);
		}
		else{
			left = mid + 1;						//update left
		}
	}

	//printf("last cur = %d\n", cur);
	return cur;
}


int main()
{
	int i, j, T, N, K, max, ans, ans2;
	scanf("%d", &T);
	while(T--){
		scanf("%d%d", &N, &K);
		vector<int> location(N + 1);
		for(i = 1; i <= N; i++){
			scanf("%d", &location[i]);
		}
		sort(location.begin(), location.end());	//sort the cities
		ans = b_search(0, location[N], K, N, location);
		printf("%d\n",ans);
	}
	return 0;
}