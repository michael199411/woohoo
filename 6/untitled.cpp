#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

int deno[12] = {1, 5, 10, 20, 50, 100, 100, 200, 500, 1000, 1000, 2000};

int main()
{
	int i, j, T, price;
	scanf("%d", &T);
	while(T--){
		int money[10], sum = 0, count = 0, flag = 0, last_sum, c_deno;
		int price2;
		scanf("%d", &price);
		for(i = 0; i < 10; i++){
			scanf("%d", &money[i]);
			count += money[i];
			sum += money[i] * deno[i];
			if(sum >= price && flag == 0){
				flag = 1;
				c_deno = i;
				price2 = sum - price;
			}
		}
		if(price2 < 0)
			printf("-1\n");
		else if(price2  == 0)
			printf("%d\n", count);
		else{
			for(i = c_deno; i >= 0; i--){
				while(money[i] > 0){
					price2 = price2 - money[i] * deno[i];
					count--;
					money[i]--;
					if(price2 == 0){
						printf("%d\n", count);
						flag = 2;
						break;
					}
				
				} 	
				if(flag == 2)
					break;
			}
			if(price2 != 0)
				printf("-1\n"); 
		}

	}
	return 0;
}
