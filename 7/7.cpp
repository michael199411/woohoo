#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

int deno[10] = {1, 5, 10, 20, 50, 100, 200, 500, 1000, 2000};	//the value of money

int main()
{
	int i, j, T, p;
	scanf("%d", &T);
	while(T--){
		int money[10], sum = 0, count = 0, fake[10] = {0}, tmp, tmp2, ori, tmp50, tmp500;
		int answer[4] = {-10, -10, -10, -10}, ans = 0;
		scanf("%d", &p);
		for(i = 0; i < 10; i++){
			scanf("%d", &money[i]);
			count += money[i];				//total number of money
			sum += money[i] * deno[i];		//total money
		}
		tmp50 = money[5];
		tmp500 = money[8];
		sum -= p; 							//total money - price 
		ori = sum;
		if(sum < 0) {						//total money is not enough
			printf("-1\n");
			continue;
		}
		if(sum == 0){
			printf("%d\n", count);
			continue;
		}
		else{
			//condition : take 50 and 500
			if(money[4] != 0 && money[7] != 0){		//必須有50and500
				sum = ori;			
				ans = 2;					//take one 50 and one 500
				sum = sum - 50 - 500;		
				for(i = 9; i >= 0; i--){
					if(sum >= 0){	
						if(deno[i] == 50 || deno[i] == 500){			//50 and 500 already change to 100 and 1000
							continue;
						}
						else if(deno[i] == 100 || deno[i] == 1000){		//change 50&500 to 100&1000
							fake[i] = (money[i - 1] - 1) / 2;			//-1因為一開始已經拿過50或500
							money[i] += fake[i];						
							tmp = sum / deno[i];						//拿幾個Ｘ元
							if(tmp > money[i]) tmp = money[i];
							sum -= tmp * deno[i];
							if(tmp >= (money[i] - fake[i])){			//fake 100 = two 50 dollars
								tmp2 = tmp - (money[i] - fake[i]);		//先拿正常100
								ans += (money[i] - fake[i]) + (tmp2) * 2;
							}
							else{
								ans += tmp;
							}
						}
						else if(money[i] != 0){
							tmp = sum / deno[i];						
							if(tmp > money[i]) tmp = money[i];			
							sum = sum - tmp * deno[i];
							ans += tmp;
						}
					}
				}	//end for loop
				if(sum != 0 )
					answer[0] = -1;				//無法湊出正確錢數
				else
					answer[0] = count - ans;	
			}
			//condition : 都不拿--------------------------
			money[5] = tmp50;				//初始化
			money[8] = tmp500;
			sum = ori;
			ans = 0;
			for(i = 9; i >= 0; i--){
				if(sum >= 0){
					if(deno[i] == 50 || deno[i] == 500)
						continue;
					else if(deno[i] == 100 || deno[i] == 1000){		
						fake[i] = (money[i - 1]) / 2;	
						money[i] += fake[i];					
						tmp = sum / deno[i];
						if(tmp > money[i]) tmp = money[i];
						sum -= tmp * deno[i];
						if(tmp >= (money[i] - fake[i])){
							tmp2 = tmp - (money[i] - fake[i]);	
							ans += (money[i] - fake[i]) + (tmp2) * 2;
						}
						else{
							ans += tmp;
						}
					}
					else if(money[i] != 0){
						tmp = sum / deno[i];						
						if(tmp > money[i]) tmp = money[i];			
						sum = sum - tmp * deno[i];
						ans += tmp;
					}
				}
			}
			if(sum != 0)
				answer[1] = -1;
			else
				answer[1] = count - ans;

			//只拿50不拿500-------------------------
			money[5] = tmp50;
			money[8] = tmp500;
			if(money[4] != 0){						//必須有50
				sum = ori;
				ans = 1;
				sum -= 50;
				for(i = 9; i >= 0; i--){
					if(sum >= 0){
						if(deno[i] == 500 || deno[i] == 50)
							continue;
						else if(deno[i] == 100){		
							fake[i] = (money[i - 1] - 1) / 2;	
							money[i] += fake[i];				
							tmp = sum / deno[i];
							if(tmp > money[i]) tmp = money[i];
							sum -= tmp * deno[i];
							if(tmp >= (money[i] - fake[i])){
								tmp2 = tmp - (money[i] - fake[i]);	
								ans += (money[i] - fake[i]) + (tmp2) * 2;
							}
							else{
								ans += tmp;
							}
						}
						else if(deno[i] == 1000){		
							fake[i] = (money[i - 1]) / 2;	
							money[i] += fake[i];					
							tmp = sum / deno[i];
							if(tmp > money[i]) tmp = money[i];
							sum -= tmp * deno[i];
							if(tmp >= (money[i] - fake[i])){
								tmp2 = tmp - (money[i] - fake[i]);	
								ans += (money[i] - fake[i]) + (tmp2) * 2;
							}
							else{
								ans += tmp;
							}
						}
						else if(money[i] != 0){
							tmp = sum / deno[i];						
							if(tmp > money[i]) tmp = money[i];			
							sum = sum - tmp * deno[i];
							ans += tmp;
						}

					}
				}
				if(sum != 0)
					answer[2] = -1;
				else
					answer[2] = count - ans;
			}
			//只拿500不拿50-------------------------
			money[5] = tmp50;
			money[8] = tmp500;
			if(money[7] != 0){
				sum = ori;
				ans = 1;
				sum -= 500;
				for(i = 9; i >= 0; i--){
					if(sum >= 0){
						if(deno[i] == 50 || deno[i] == 500)
							continue;
						else if(deno[i] == 1000){		
							fake[i] = (money[i - 1] - 1) / 2;	
							money[i] += fake[i];					
							tmp = sum / deno[i];
							if(tmp > money[i]) tmp = money[i];
							sum -= tmp * deno[i];
							if(tmp >= (money[i] - fake[i])){
								tmp2 = tmp - (money[i] - fake[i]);	
								ans += (money[i] - fake[i]) + (tmp2) * 2;
							}
							else{
								ans += tmp;
							}
						}
						else if(deno[i] == 100){		
							fake[i] = (money[i - 1]) / 2;	
							money[i] += fake[i];					
							tmp = sum / deno[i];
							if(tmp > money[i]) tmp = money[i];
							sum -= tmp * deno[i];
							if(tmp >= (money[i] - fake[i])){
								tmp2 = tmp - (money[i] - fake[i]);	
								ans += (money[i] - fake[i]) + (tmp2) * 2;
							}
							else{
								ans += tmp;
							}
						}
						else if(money[i] != 0){
							tmp = sum / deno[i];						
							if(tmp > money[i]) tmp = money[i];			
							sum = sum - tmp * deno[i];
							ans += tmp;
						}

					}
				}
				if(sum != 0)
					answer[3] = -1;
				else
					answer[3] = count - ans;
			}
		int max = -1;
		for(i = 0; i < 4; i++){
			printf("%d\n", answer[i]);
			if(max < answer[i])
				max = answer[i];
		}
		printf("%d\n", max);
	}

	}
	return 0;
}
