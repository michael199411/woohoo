#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <queue>
using namespace std;

struct room{
	char status;
	int x, y, z;
	int bev;
	int dis;
	int visit;
};

int main()
{
	int i, j, k, y, sx, sy, sz;
	int T;
	
	scanf("%d", &T);
	while(T--){
		int n, height = 0;
		room map[100][100][100];
		char buf[120];
		queue< room > bfs_queue;
		room start, temp, next, end;
		end.visit = 0;
		scanf("%d", &n);
		for(i = 0; i < n * n; i++){
			y = i % n;
			height = i / n;
			scanf("%s", buf);
			for(j = 0; j < n; j++){
				map[height][y][j].status = buf[j];
				if(buf[j] == 'B')						//already has one beverage
					map[height][y][j].bev = 1;
				else
					map[height][y][j].bev = 0;
				map[height][y][j].dis = 0;
				map[height][y][j].visit = 0;
				map[height][y][j].x = j;
				map[height][y][j].y = y;
				map[height][y][j].z = height;

				if(buf[j] == 'S'){
					start = map[height][y][j];			//start room
					bfs_queue.push(start);
				}
			}
		}

		while(bfs_queue.empty() != 1){
			temp = bfs_queue.front();					//dequeue 
			temp = map[temp.z][temp.y][temp.x];			//update the element in case it was modified
			bfs_queue.pop();

			// +X direction , test if the room is in the cube and is there a trap in the room
 			if((temp.x + 1) < n && (map[temp.z][temp.y][temp.x + 1].status != '#') && (map[temp.z][temp.y][temp.x + 1].status != 'S')){
				next = map[temp.z][temp.y][temp.x + 1];
				
				if(next.visit != 1){				//not visited room
					next.dis = temp.dis + 1;		//update distance to the start room
					next.bev += temp.bev;			//update the beverage
					bfs_queue.push(next);			//enqueue
					next.visit = 1;
				}
				else if(next.visit == 1){			//if visited, test if there is a better answer
					if((temp.dis + 1 == next.dis) && next.status == 'B'){
						if(temp.bev + 1 > next.bev)
							next.bev = temp.bev + 1;
					}
					else if((temp.dis + 1 == next.dis) && next.status != 'B')
						if(temp.bev > next.bev)
							next.bev = temp.bev;
				}
				if(next.status == 'E')				//remember the room if it is exit
						end = next;
				map[temp.z][temp.y][temp.x + 1] = next;		//update the information of the room
				
			}
			//GO +Y direction
			if((temp.y + 1) < n && (map[temp.z][temp.y + 1][temp.x].status != '#') && (map[temp.z][temp.y + 1][temp.x].status != 'S')){
				next = map[temp.z][temp.y + 1][temp.x];
				
				if(next.visit != 1){
					next.dis = temp.dis + 1;
					next.bev += temp.bev;
					bfs_queue.push(next);
					next.visit = 1;

				}
				else if(next.visit == 1){
					if((temp.dis + 1 == next.dis) && next.status == 'B'){
						if(temp.bev + 1 > next.bev)
							next.bev = temp.bev + 1;
					}
					else if((temp.dis + 1 == next.dis) && next.status != 'B')
						if(temp.bev > next.bev)
							next.bev = temp.bev;
				}
				if(next.status == 'E')
						end = next;
				map[temp.z][temp.y + 1][temp.x] = next;
			}
			//GO +Z direction
			if((temp.z + 1) < n && (map[temp.z + 1][temp.y][temp.x].status != '#') && (map[temp.z + 1][temp.y][temp.x].status != '#')){
			//	printf("in 3\n");
				next = map[temp.z + 1][temp.y][temp.x];
				
				if(next.visit != 1){
					next.dis = temp.dis + 1;
					next.bev += temp.bev;
					bfs_queue.push(next);
					next.visit = 1;

				}
				else if(next.visit == 1){
					if((temp.dis + 1 == next.dis) && next.status == 'B'){
						if(temp.bev + 1 > next.bev)
							next.bev = temp.bev + 1;
					}
					else if((temp.dis + 1 == next.dis) && next.status != 'B')
						if(temp.bev > next.bev)
							next.bev = temp.bev;
				}
				if(next.status == 'E')
						end = next;
				map[temp.z + 1][temp.y][temp.x] = next;
			}
			//GO -X direction
			if((temp.x - 1) >= 0 && (map[temp.z][temp.y][temp.x - 1].status != '#') && (map[temp.z][temp.y][temp.x - 1].status != 'S')){
				next = map[temp.z][temp.y][temp.x - 1];
				
				if(next.visit != 1){
					next.dis = temp.dis + 1;
					next.bev += temp.bev;
					bfs_queue.push(next);
					next.visit = 1;

				}
				else if(next.visit == 1){
					if((temp.dis + 1 == next.dis) && next.status == 'B'){
						if(temp.bev + 1 > next.bev)
							next.bev = temp.bev + 1;
					}
					else if((temp.dis + 1 == next.dis) && next.status != 'B')
						if(temp.bev > next.bev)
							next.bev = temp.bev;
				}
				if(next.status == 'E')
						end = next;
				map[temp.z][temp.y][temp.x - 1] = next;
			}
			//GO -Y direction
			if((temp.y - 1) >= 0 && (map[temp.z][temp.y - 1][temp.x].status != '#') && (map[temp.z][temp.y - 1][temp.x].status != 'S')){
			//	printf("in 5\n");
				next = map[temp.z][temp.y - 1][temp.x];
				
				if(next.visit != 1){
					next.dis = temp.dis + 1;
					next.bev += temp.bev;
					bfs_queue.push(next);
					next.visit = 1;
				}
				else if(next.visit == 1){
					if((temp.dis + 1 == next.dis) && next.status == 'B'){
						if(temp.bev + 1 > next.bev)
							next.bev = temp.bev + 1;
					}
					else if((temp.dis + 1 == next.dis) && next.status != 'B')
						if(temp.bev > next.bev)
							next.bev = temp.bev;
				}
				if(next.status == 'E')
						end = next;
				map[temp.z][temp.y - 1][temp.x] = next;
			}
			//GO -Z direction
			if((temp.z - 1) >= 0 && (map[temp.z - 1][temp.y][temp.x].status != '#') && (map[temp.z - 1][temp.y][temp.x].status != 'S')){
				next = map[temp.z - 1][temp.y][temp.x];
				
				if(next.visit != 1){
					next.dis = temp.dis + 1;
					next.bev += temp.bev;
					bfs_queue.push(next);
					next.visit = 1;
	
				}
				else if(next.visit == 1){
					if((temp.dis + 1 == next.dis) && next.status == 'B'){
						if(temp.bev + 1 > next.bev)
							next.bev = temp.bev + 1;
					}
					else if((temp.dis + 1 == next.dis) && next.status != 'B')
						if(temp.bev > next.bev)
							next.bev = temp.bev;
				}
				if(next.status == 'E')
						end = next;
				map[temp.z - 1][temp.y][temp.x] = next;
			}
		}

		if(end.visit == 0)
			printf("Fail OAQ\n");
		else{
			printf("%d %d\n", end.dis, end.bev);
		}

	}
	return 0;
}