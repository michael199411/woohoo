#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
using namespace std;

struct node{
	int id;
	int weight;
};

vector< vector< node > > adj_matrix(100100);	
vector< int > work_chart(100100, 0);			//whether node is visited


void DFS(node u, int value)		//DFS 				
{
	int i, j;
	work_chart[u.id] = 1;
	for(i = 0; i < adj_matrix[u.id].size(); i++){
		if(( (adj_matrix[u.id][i].weight | value) <= value) && (work_chart[adj_matrix[u.id][i].id] == 0)){
			DFS(adj_matrix[u.id][i], value);
		}
	}
}

int main()
{
	int i, j, k = 0, n, m, T, max = 0, count = 0;
	node temp;
	scanf("%d", &T);
	while(T--){
		int a, b, c;
		scanf("%d%d", &n, &m);
		for(i = 0; i < m; i++){
			scanf("%d%d%d", &a, &b, &c);
			if(c > max)	max = c;
			temp.id = b - 1;
			temp.weight = c;
			adj_matrix[a - 1].push_back(temp);
			temp.id = a - 1;
			adj_matrix[b - 1].push_back(temp);
		}
		
		int left = 0, right = max, mid;
		while((1 << k) <= right){			//the upper bound
			k++;
		}
		right = (1 << k) - 1;
		while(left < right){				//binary search
			count = 0;	
			mid = (left + right) / 2;
			DFS(temp, mid);
			for(i = 0; i < n; i++){			//test if connected
				if(work_chart[i] == 0)
					count = 1;
			}
			if(count == 1){					//not connected
				left = mid + 1;
			}
			else{							//find smaller value
				right = mid;
			}
			for(j = 0; j < n; j++)			//clean the vector, set all node unvisited
				work_chart[j] = 0;
		}

		printf("%d\n", right);
		for(i = 0; i < n; i++){				//initialize the adj_matrix
			adj_matrix[i].clear();
		}
	}
	return 0;
}